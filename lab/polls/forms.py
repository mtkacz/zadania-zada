from django.forms import ModelForm
from models import Poll


class PollForm(ModelForm):
    class Meta:
        model = Poll


class PartialPollForm(ModelForm):
    class Meta:
        model = Poll
        fields = ('question',)

# class LikeForm(ModelForm):
#     class Meta:
#         model = Like
#
#
# class PartialLikeForm(ModelForm):
#     class Meta:
#         model = Like
#         fields = ('ile',)